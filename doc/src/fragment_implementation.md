Fragment Implementation - danshi Documentation
==============================================
Fragments are the main part of the data abstraction layer. They keep track of a 
region of data and how it is represented in the system. A fragment has a
backend, which describes the type of data it is and the operations that can be
performed on it, and a size, which is how big it is. 

Referencing Fragments
---------------------
Fragments are referenced either directly through their memory address or through 
a fragment pointer, which uses a lookup table to translate the fragment pointer 
into an actual memory pointer. The reason fragment pointers are needed is that
in the Virtual Process Image, if a page is mapped, but the fragment supporting
it is not loaded, it needs to have some kind of reference to the fragment which
supports it, as well as other info, all in 4 bytes. So, we use the lookup table 
to translate a smaller number into the actual address of the fragment which 
supports this part of the virtual address space. A fragment pointer only takes 
2 bytes, leaving the other 2 for use by the virtual process image (and the 
paging hardware). 

### The Global Fragment Pointer Table (GFPT)
The Global Fragment Pointer Table, for GFPT, serves as a pointer lookup table to 
fragment structures. Each slot in the fragment pointer table contains a pointer 
to a fragment structure in memory. The table is actually a 2 level structure,
made of a Pointer Directory which contains the addresses of Pointer tables, so 
that depending on system usage, the amount of available fragment pointers can
change. Both structures each consist of 256 entries, each entry being 4 bytes, 
to make a total size of 1024 bytes for the directory and 1024 bytes per entry. 
Note that these structures need to be 4-byte aligned, since the bottom 2 bits of 
the Pointer Directory are reserved. The bottom 2 bits of the tables are also 
reserved, meaning that the fragment structures themselves must also be aligned. 

### Structure of a GFPT directory entry
`[00:00]` Present bit, denoting whether the pointer is valid.<br>
`[01:01]` Reserved<br>
`[31:02]` Pointer to a GFPT table entry

### Structure of a GFPT table entry
`[00:00]` Present bit, denotes this is a valid fragment pointer<br>
`[01:01]` Reserved<br>
`[31:02]` Pointer to a fragment structure

Fragment Structure
------------------
A fragment has a pretty simple structure. The first 4 bytes contain the backend 
of the fragment, it's type and allowed operations. The next 4 bytes denote the
size, in bytes. Note that this is *usually* page aligned (not always, it may
need to be rounded to page size). The next 4 bytes are the Immediate status
bytes, which have a variable meaning, but are ususally used to store quick
access flags about the fragment, such as whether it is loaded or mapped. The
final 4 bytes are *usually* a pointer to a structure storing more information
about the fragment, used by its backend to point to keep some state information
on the fragment. I say usually, because if the fragment is simple enough, it
might only need 8 bytes to store its state, and so a pointer would not be 
needed.

In addition to this, all fragments support some global operations 
interface-wise, so the convention is to use the 2 lowest bits of the Immediate
Status Bytes to keep track of backend independent state, such as whether the
fragment is currently in the "Loaded" state or not. 

Universal Fragment Operations
-----------------------------
There are several operations which can be performed on ALL fragments. Note that
this doesn't mean the same code is run regardless of fragment type, but that all
fragments have some way of carrying out the functionality of these functions. 
There is a distinct difference between the *operations* a fragment supports and
the *functions* it can be used with to implement those operations. The universal
fragment operations are:

### `fragment_pointer_t fragment_create(backend_t backend, ...);`
This initialises the fragment, gives it a slot in the GFPT and runs any 
initialisation required for the backend of this fragment. It takes an argument
specifiying the backend, followed by backend-specific initialisation arguments.
Note that this shouldn't put the fragment into a default state, but just make it
functional enough that it can be initialised or destroyed properly. The 
fragment should now be considered in the `valid` state.

### `void fragment_init(fragment_pointer_t f_pointer, ...);`
This function should be the one which actually initialises the fragment, which
should include determining its size, which is undefined until this function is
run. Return values should be done through pass by reference. The fragment should
now be in what is called the `unloaded` state. 

### `void fragment_load(fragment_pointer_t f_pointer);`
This function should put the fragment in the `loaded` state. What this does is 
largely backend independent, but in an abstract sense, it should allow the 
fragment to begin to consume physical memory. As well as this, it would be a 
good idea for the fragment to prepare to be mapped and accessed, eg. it should 
probably do something like load the start of a file from a disk or something 
like that. 

### `void fragment_unload(fragment_pointer_t f_pointer);`
This function should put the fragment back into the `unloaded` state, freeing 
any physcial memory it is using in the process. Depending on the backend, this 
may have the side effect of doing something such as swapping out the data it
contained to disk, but not always. However, it always garuntees that whatever 
physical memory the fragment was using was freed. 

### `void fragment_uninit(fragment_pointer_t f_pointer);`
This function should uninitialise the fragment. Similar to `fragment_unload`, it 
should perform any buffered writes or other finalizations and then put the 
fragment back into the `valid` state. You don't have to free any structures used 
by the fragment, just make sure that it's no longer in a usable state. 

### `void fragment_destroy(fragment_pointer_t);`
This should free up any heap memory used by the fragment structure, clean out 
its GFPT slot and make it invalid. Now the fragment pointer is effectively a 
null pointer. 

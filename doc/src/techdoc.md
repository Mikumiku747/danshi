Technical Documentation - danshi Documentation
==============================================

System Implementation Theory
----------------------------
This is supposed to be the theory of how I implement certain parts of the
system. It's really just self memos, so don't try to take it too seriously. It
will also probably be wrong because I like to code more than I like to write
documentation on my own works...

### (Memory Abstraction Model)[./mem_abstraction.html]
danshi uses virtual memory, which means that I need to abstract away the system
memory. Often in several ways. So this document outlines how exactly I do that.
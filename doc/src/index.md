danshi Documentation
====================

danshi is an x86 operating system project. Currently it just consists of a
kernel, but in future I hope to add a userspace, make it self hosting and port
some software. 

This is the documentation for the project. The project is split up into several
parts, the main part being the kernel. This part acts as documentation, both on
how the kernel interacts with the rest of the system, as well as user docs and
technical notes which describe the inner workings of the system. 

User Documentation
------------------
This is for people who want to use the system. Keep in mind that since this is a
hobby project, I'm mostly just providing this as a quick start for the other
devs who want to try out the system. 

- Building and installation

Developer Documentation
-----------------------
This is for people who want to write kernel modules or native software for the 
system. Right now it's pretty much out of order, since there's no userspace or
kernel module support right now. Maybe a little later on...

Technical Documentation 
-----------------------
Beware, most of this is just internal doc for myself as a developer, so it would
be a bad idea to read any of it and expect to learn anything. You might, but you
probably won't, it's going to be bad.

- Memory Abstraction Model (and to a lesser extent, data abstraction in the
system in general...)

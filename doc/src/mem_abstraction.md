Memory Abstraction Model - danshi Documentation
===============================================

The memory abstraction model needs to be pretty good, considering it's the 
backbone of the system. It's mainly split into 3 parts, the physical memory
pool, the virtual process image, and a layer which sandwiches between them, the 
data abstraction layer. The middle part is the most important, since it's not 
only used for memory abstraction, but also shared memory and memory based IO. 

Data abstraction layer
----------------------
### Fragments

The data abstraction layer forms the backend of the operating system, and it is
how programs and the kernel interact with each other and the hardware. The
abstraction contains objects called fragments. Each fragment represents some
kind of data entity, be it a file, some memory space, some shared memory, or
even the stack of a program. A fragment has a size, which may or may not be 
changeable, as well as a backend, which describes the kind of data the fragment
contains and tells the system how to operate on the data. 

### Fragment operations

There are generic operations that can be done on fragments, such as mapping them
into a virtual address space, and there are specific operations which can only 
be performed on an object with a certain backend, for example, swapping out some
pages of a data area. the operations which can be done on a fragment depend
entirely on the backend the fragment uses. 

### Fragment Backends
The backend of a fragment are the system resources which are actually used to
implement the functionality of a fragment. While all virtual memory maps to some
physical memory, some of it may be used for differnt purposes or have different
properties. The backend of a fragment can be thought of as the type of it, as 
well as the interface to it, if you like to think in OO terms. 

### Fragment Specifics
Since the nature of fragments varies so wildly, they have a simple extensible
representation. The structure of a fragment is made up of a backend descriptor,
which describes exactly the type and supported operations of a certain fragment,
and a size, which describes the size of the fragment, usually a multiple of 4KiB
so that it works with the architecture easily (though certainly not always). The
size of the fragment is certain at any given point, and while for some backends
it can be changed, for others it is fixed. They also almost always contain a
pointer to some kind of extra data structure, though depending on the backend it
might not be needed, and a pointer to the next fragment in a linked list of 
them, depending on the usage.

Virtual Process Image
---------------------

The virtual process image is collectively the page directories and page tables 
which the MMU uses to translate addresses. It is used in combination with the
data abstraction layer to present information to the kernel and the currently
running process. 

### Fragments and Paging
Since the x86 architecture uses paging for virtual memory, and page faults when
an illegal memory access occurs, it's common practice to keep the information
needed to resolve a page fault in the page tables/directories. In this case, 
the page directory entry contains some kind of fragment pointer, which indicates
a specific fragment which should be mapped to that location. Based on that, and
the nature of the fault, the system can decide what to do next. For example, if 
I had swapped out a part of a program text fragment, the system would page fault
when I tried to read the address it had been mapped to. I would then use the
faulting address to locate the relevant entry in the virtual process image, and
discover that there is indeed a fragment mapped to this location, since the 
fragment pointer is present. The next order of action would be to look it up in
the kernel's data structures using the fragment pointer, and take the
appropriate action. In the case of our program text, it would be to allocate
some physical memory from the physical memory pool, fill it with the contents
requested from the file, and then map it into the place where the fault occured,
which would resolve the fault, and the process could be resumed.

### Process Image Specifics
Since there is a limited amount of not only memory, but address space in the x86
architecture, it's important that the system is conservative with address space,
since once it runs out, there is no way to get more, and the system will fail.
Therefore, each process gets its own process image, which is completely empty
apart from anything the process itself uses and the necessary kernel objects in
kernel address space, which use page tables common to every process. Something
to note is that while the page directory is kept in kernel space, the individual
page tables that the process uses are kept in the user address space, though
they are still only accessible by the kernel, and can be swapped out at any
time. This saves the kernel some address space, since userspace page tables are
unique and useful only to user processes.

Physical Memory Pool
--------------------

The main supplier of backend functionality is the Physical memory pool. It keeps 
track of which physical pages in the system are actually being used by anything 
and which are just idle and unused. It forms one of the parts of the backend 
functionality which is used to implement fragments. It's important to note that 
the only thing the physical memory pool does is keep track of usage, and not who 
it is being used by or what it is being used for. It's up to the fragments and 
thier backend code to keep track of who is actually using some physical memory. 

The good thing about the physical memory pool is that it allows you to ask for 
an arbitrarily sized chunk of memory, and it can either  return a contiguous run 
of physical memory or a series of runs of pages, which you can then use with the 
garuntee that you have ownership of that memory and nobody else can edit or use 
it. However, it's important that if the system asks, you either give up that 
physical memory or temporarily relinquish it (eg. swap to disk). 



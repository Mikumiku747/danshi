User Documentation - danshi Documentaion
========================================

Building
--------

### Required Tools
Currently, the kernel is just built into a 32-bit multiboot elf. You can use
grub to make it bootable, or you can boot it yourself (although it expects a
multiboot environment, and you need to be able to provide certain multiboot
features the kernel expects, grub is the easiest way to do this).

To build the kernel from source, you'll need some tools:
- A gcc cross compiler targeting `i686-elf-none` or some other freestanding 
cross compiler. 
- A standard linux build environment (ie. bash or similar) and GNU `make` or 
something equally powerful.
- The NASM assembler or some other NASM-compatible assembler (or if you're a 
truly sadistic person, you can translate the assembly...don't.)

That's just the stuff for building the kernel binary. You'll need even more to 
make a bootable disk image:
- grub-mkrescue or some other way of booting the kernel (just use grub, it's the
best way).

### Building
To start with, you're going to want to open up the `Makefile` and take a look
inside, particularly at the part with the tools. If your toolset doesn't match
my one exactly (it probably doesn't) you should set the values to the correct
tools. 

Next, your build will undoubtedly fail for some reason, probably because again,
your environment is configured differently to mine. Check the flags section and
make sure that all the correct arguments are getting passed to the tools, 
especially if you're using different tools to me. 

Finally, I should note that anything that's not explicitly marked as a release 
of some kind should be considered 100% unreliable and broken. 

# Makefile
# Daniel Selmes 2016
# The makefile for the project.

# DIRECTORIES
# Definitions for directory names for flexibility
# Kernel Directories
KERN_DIR := kernel
KERN_SRC_DIR := $(KERN_DIR)/src
KERN_OBJ_DIR := $(KERN_DIR)/obj
KERN_BIN_DIR := $(KERN_DIR)/bin
KERN_INC_DIR := $(KERN_DIR)/include
# System image directories
SYSROOT_DIR := sys
SYS_INC_DIR := $(SYSROOT_DIR)/include
SYS_BOOT_DIR := $(SYSROOT_DIR)/boot
# Grub directories
GRUB_DIR := grub
# Documentation directories
DOC_DIR := doc
DOC_SRC_DIR := $(DOC_DIR)/src
DOC_HTML_DIR := $(DOC_DIR)/html

# TOOLSETS
# Define the toolset used to make the kernel
KERN_CROSS := i686-elf
KERN_CC := $(KERN_CROSS)-gcc
KERN_AS := nasm
KERN_LD := $(KERN_CROSS)-gcc
# Define the options used to compile the kernel C sources
KERN_C_OPTS ?= -g -fno-omit-frame-pointer
KERN_C_OPTS := $(KERN_C_OPTS) -std=c11 -ffreestanding -Wall -Wextra
KERN_C_INCS ?= 
KERN_C_INCS := $(KERN_C_INCS) -I$(KERN_INC_DIR)
KERN_C_LIBS ?=
KERN_C_LIBS := $(KERN_C_LIBS)
# Define the options used to compile the kernel asm sources
KERN_AS_OPTS ?= -g
KERN_AS_OPTS := $(KERN_AS_OPTS) -felf32
KERN_ASFLAGS := $(KERN_AS_OPTS)
# Final cflags
KERN_CFLAGS := $(KERN_C_OPTS) $(KERN_C_INCS) $(KERN_C_LIBS)
# Define the options used to link the kernel
KERN_LINK_OPTS ?= -g
KERN_LINK_OPTS := $(KERN_LINK_OPTS) -ffreestanding -nostdlib
KERN_LINK_LIBS ?= 
KERN_LINK_LIBS := $(KERN_LINK_LIBS) -lgcc
KERN_LFLAGS := $(KERN_LINK_OPTS)

# Define the tools used to make the documentation
MARKDOWN ?= markdown
MARKDOWN := $(MARKDOWN)

# FILESETS
# Lists of files to be compiled/built/whatever

# Kernel filesets
KERN_C_SOURCES := $(wildcard $(KERN_SRC_DIR)/*.c)
KERN_S_SOURCES := $(wildcard $(KERN_SRC_DIR)/*.s)
KERN_C_OBJS := $(patsubst $(KERN_SRC_DIR)/%.c, $(KERN_OBJ_DIR)/%.o, $(KERN_C_SOURCES))
KERN_S_OBJS := $(patsubst $(KERN_SRC_DIR)/%.s, $(KERN_OBJ_DIR)/%.o, $(KERN_S_SOURCES))
KERN_OBJS := $(KERN_C_OBJS) $(KERN_S_OBJS)

# Documentation filesets
DOC_MD_SOURCES := $(wildcard $(DOC_SRC_DIR)/*.md)
DOC_HTML_FILES := $(patsubst $(DOC_SRC_DIR)/%.md, $(DOC_HTML_DIR)/%.html, $(DOC_MD_SOURCES))

# RULES
# How to actually make files

# Project rules
all: danshi.iso

.PHONY: all clean

danshi.iso: $(SYSROOT_DIR) $(KERN_BIN_DIR)/kernel.bin $(GRUB_DIR)/grub.cfg
	grub-mkrescue -o danshi.iso $(SYSROOT_DIR) -- --quiet

$(SYSROOT_DIR): $(KERN_BIN_DIR)/kernel.bin $(GRUB_DIR)/grub.cfg
	mkdir -p $(SYSROOT_DIR)
	mkdir -p $(SYS_BOOT_DIR)/grub
	cp $(GRUB_DIR)/grub.cfg $(SYS_BOOT_DIR)/grub/grub.cfg
	cp $(KERN_BIN_DIR)/kernel.bin $(SYS_BOOT_DIR)/kernel.bin

# Kernel Rules
$(KERN_BIN_DIR)/kernel.bin: $(KERN_BIN_DIR) $(KERN_OBJS)
	$(KERN_LD) -T $(KERN_DIR)/kernel_link.ld -o $(KERN_BIN_DIR)/kernel.bin $(KERN_LFLAGS) $(KERN_OBJS) $(KERN_LINK_LIBS)

$(KERN_BIN_DIR):
	mkdir -p $(KERN_BIN_DIR)

$(KERN_OBJ_DIR)/%.o: $(KERN_SRC_DIR)/%.c $(KERN_OBJ_DIR)
	$(KERN_CC) $(KERN_CFLAGS) -c $< -o $@ $(KERN_C_LIBS)
$(KERN_OBJ_DIR)/%.o: $(KERN_SRC_DIR)/%.s $(KERN_OBJ_DIR)
	$(KERN_AS) $(KERN_AS_OPTS) $< -o $@

$(KERN_OBJ_DIR):
	mkdir -p $(KERN_OBJ_DIR)

doc: $(DOC_HTML_DIR) $(DOC_HTML_FILES)

$(DOC_HTML_DIR)/%.html: $(DOC_SRC_DIR)/%.md $(DOC_HTML_DIR)
	$(MARKDOWN) < $< > $@

$(DOC_HTML_DIR):
	mkdir -p $(DOC_HTML_DIR)


clean:
	rm -rfv $(SYSROOT_DIR)
	rm -rfv $(KERN_OBJ_DIR)
	rm -rfv $(KERN_BIN_DIR)
	rm -rfv danshi.iso
	rm -rfv $(DOC_HTML_DIR)


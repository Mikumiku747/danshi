/* multiboot.h
 * Daniel Selmes 2016
 * Multiboot structures and other asscociated data. */

/* Include protection. */
#ifndef MULTIBOOT_H
#define MULTIBOOT_H

/* Multiboot magic values. */
#define MULTIBOOT_HEADER_MAGIC 0x1BADB002
#define MULTIBOOT_INFO_MAGIC 0x2BADB002

/* Multiboot information structure. */
struct multiboot_info_struct {
	uint32_t flags;
	
	uint32_t mem_lower;
	uint32_t mem_upper;
	
	uint32_t boot_device;
	
	uint32_t cmdline;
	
	uint32_t modules_count;
	uint32_t modules_addr;
	
	uint32_t sym0;
	uint32_t sym1;
	uint32_t sym2;
	uint32_t sym3;
	
	uint32_t mmap_length;
	uint32_t mmap_addr;
	
	uint32_t drives_length;
	uint32_t drives_addr;
	
	uint32_t config_table;
	
	uint32_t bootloader_name;
	
	uint32_t apm_table;
	
	uint32_t vbe_control_info;
	uint32_t vbe_mode_info;
	uint32_t vbe_mode;
	uint32_t vbe_interface_seg;
	uint32_t vbe_interface_off;
	uint32_t vbe_interface_len;
};

typedef struct multiboot_info_struct multiboot_info_t;

/* Multiboot module info structure. */
struct multiboot_modstruct {
	uint32_t module_start;
	uint32_t module_length;
	uint32_t modstring;
	uint32_t mod_reserved;
};

typedef struct multiboot_modstruct multiboot_modstruct_t;

#endif
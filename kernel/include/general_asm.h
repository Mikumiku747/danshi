/* general_asm.h
 * Daniel Selmes 2016
 * General miscellaneous assembly functions used by the kernel
*/

/* Include protection. */
#ifndef GENERAL_ASM_H
#define GENERAL_ASM_H

/* hang
 * Clears interrupts and hangs the kernel. */
extern void hang();

/* halt
 * Halts the processor until it receieves an interrupt. */
extern void halt();

#endif

/* kernel.c
 * Daniel Selmes 2016
 * The main kernel. The big fish. This is what runs the system. 
*/

/* System includes. */
#include <stdint.h>
/* Kernel includes. */
#include <multiboot.h>
#include <general_asm.h>

void kernel_main(uint32_t magic, multiboot_info_t *multiboot_info) 
{
	/* Check we were actually booted by a multiboot compliant bootloader. */
	if (magic != 0x2BADB002) {
		/* Hang, since we can't deal with a non-multiboot environment. */
		hang();
	}
	
	/* Adjust the physical address of the multiboot info into a virtual address. */
	multiboot_info = multiboot_info + (0xC0000000 / sizeof(multiboot_info_t));
	if ((multiboot_info->flags & 0x00000840) != 0x00000840) {
		/* Hang, since we can't deal with not supporting these flags. */
		hang();
	}
	
	/* First things first, we need to get memory manipulation systems working. */
	
	while (1) {
		/* Do nothing... */
	}
	
	
	/* The kernel should never reach here! */
	return;
}

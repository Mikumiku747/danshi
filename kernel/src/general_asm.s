; general_asm.s
; Daniel Selmes 2016
; General miscellaneous assembly functions. 

; Expose the functions we make to the rest of the kernel
global hang, halt

section .text
hang:
	cli			; Clear interrupts so we don't get... interrupted.
	hlt 			; Halt the processor till the next interrupt.
	jmp hang 		; If we do manage to continue, try to hang again.
	
halt:
	push ebp
	mov ebp, esp 		; Create stack frame
	hlt 			; Halt the processor. 
	mov esp, ebp
	pop ebp			; Leave stack frame
	ret			; Exit

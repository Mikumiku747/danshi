; Bootstrap.s
; Daniel Selmes 2016
; The first code to be run. Sets up a higher half paging scheme and enables
; paging, as well as passing boot info into the main part of the kernel.

; Make the bootstrap global so we can set it as the entry point in the linker
global bootstrap_start, KERN_BASE_OFFSET
; Bring in the symbol for our kernel so we can jump to it when we're ready. 
extern kernel_main

; Set up the multiboot header
MB_ALIGN	equ	1 << 0 		; Ask for modules to be loaded on boundries
MB_MEMINFO	equ	1 << 1		; Ask for a memory map
MB_GRAPHICS	equ	1 << 2		; Ask for a specific video mode
MB_FLAGS	equ	MB_ALIGN | MB_MEMINFO | MB_GRAPHICS	; The flags we pass to multiboot
MB_MAGIC	equ	0x1BADB002	; Magic value so multiboot sees us
MB_CHECKSUM	equ	-(MB_MAGIC + MB_FLAGS)			; Checksum for the multiboot header
; Set up multiboot graphics preferences
MB_GFX_MODE	equ	0
MB_GFX_WIDTH	equ	1024
MB_GFX_HEIGHT	equ	768
MB_GFX_DEPTH	equ	32

section .multiboot
align 4
	dd MB_MAGIC
	dd MB_FLAGS
	dd MB_CHECKSUM
	resd 5			; Blank out the unused space in the multiboot structure
	dd MB_GFX_MODE
	dd MB_GFX_WIDTH
	dd MB_GFX_HEIGHT
	dd MB_GFX_DEPTH

; Declare some useful constants in regards to the paging system
KERN_BASE_OFFSET	equ	0xC0000000

section .bss
align 4
stack_bottom:
	resb 16384		; Reserve 16 KiB for the initial stack. 
stack_top:

align 4096
boot_pagedir:
	resb 4096
boot_pagetab0:
	resb 4096

section .text
bootstrap_start:
	; Fill in the page directories and tables
	mov edi, (boot_pagedir - KERN_BASE_OFFSET)	; We'll be filling in the first page directory entry
	mov edx, (boot_pagetab0 - KERN_BASE_OFFSET)	; Put the physical address of the first table in EDX. 
	or edx, 0x00000003				; Set the flags for R/W and present entry
	mov [edi], edx					; Put the first entry into the page directory (IDENT MAPPING)
	mov edi, (boot_pagedir - KERN_BASE_OFFSET + 0xC00) ; Fill in the 768th PD entry (starting 0xC0000)
	or edx, 0x00000003				; Set the present and R/W bits
	mov [edi], edx					; Put the 768th entry into the page dir
	; Now, to fill in the actual page table
	mov edi, (boot_pagetab0 - KERN_BASE_OFFSET)	; We'll fill in the page table
	xor ecx, ecx					; Put zero in the counter
.mkentry:
	mov edx, ecx					; Put the current index in eax
	shl edx, 12					; Make it the page number
	or edx, 0x00000003				; Set the flags
	mov [edi + ecx*4], edx				; Write the entry to the table
	inc ecx
	cmp ecx, 1024
	jne .mkentry					; Do this 1024 times
	; Now, send the directory to CR3
	mov ecx, (boot_pagedir - KERN_BASE_OFFSET)
	mov cr3, ecx					; Put the directory in CR3
	; Enable paging
	mov ecx, cr0
	or ecx, 0x80010000 				; Enable paging and page protection flags
	mov cr0, ecx
	; Jump into our paged code (Finally, running code a vaddr 0xC010something)
	mov edx, bootstrap_paged
	jmp edx
bootstrap_paged:
	; WE'RE IN HIGHER HALF YAY, let's test ourselves and unmap the lower end
	xor edx, edx					; Make a zero in edx
	mov edi, (boot_pagedir - KERN_BASE_OFFSET) 	; Select the 0th PD entry (starting 0x0)
	mov [edi], edx					; Write the zero to the entry
	mov ecx, cr0
	mov cr0, ecx					; Reload the page directory
	; Now we can prepare to go into the kernel_main
	mov esp, stack_top				; Set up the stack
	mov ebp, 0
	push ebp
	mov ebp, esp					; Set up THE stack frame ourselves.
	push ebx
	push eax					; Push the arguments to the kernel main function
	call kernel_main				; Call the kernel, ideally, should never return...
	
	jmp bs_hang					; Hang if we manage to exit the kernel...
	
bs_hang:
	; Jump here if we want to hang the cpu
	cli
	hlt
	jmp bs_hang
	
